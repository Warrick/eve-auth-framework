# Expose plugin API

class Plugin:

    #plugin initializer
    def  __init__():
        pass

    #Check if a user is registered
    def is_registered(username):
        pass

    #Register a user
    def register(username, password):
        pass

    #Update a user's registration information
    def updateRegistration(username, password):
        pass

    #Remove a registed user
    def deRegister(username):
        pass

    #Options to perform when called by cron
    def cron(valid_users, invalid_users):
        pass
