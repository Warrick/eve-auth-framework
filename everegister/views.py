from django import forms
from django.contrib.formtools.wizard.views import SessionWizardView
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseNotFound
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.template import RequestContext, loader
from everegister.models import Evecharacter, Evecorporation, Evealliance
from everegister.utils import update_eveapi, update_evecharacter, update_evecorporation, update_evealliance
from evelink import api, corp, eve
from evelink.api import APIError
from evelink import char as character
import logging


logger = logging.getLogger(__name__)


@login_required
def listAlliances(request):
    template= loader.get_template('everegister/listEntities.html')
    alliances = Evealliance.objects.all()
    context = RequestContext(request, {'title':'Alliance', 'type':'everegister:view_alliance', 'entities':alliances})
    return HttpResponse(template.render(context))


@login_required
def viewAlliance(request, alliance_id):
    try:
        template = loader.get_template('everegister/allianceProfile.html')
        evelink = eve.EVE()
        alliance = evelink.alliance(alliance_id)
        #get names for all member corps
        needNames = [corp['id'] for corp in alliance['member_corps'].values()]
        names = evelink.character_names_from_ids(needNames)
        for key in names:
            alliance['member_corps'][key]['name'] = names[key]
        context = RequestContext(request, {'alliance':alliance})
        return HttpResponse(template.render(context))
    except AttributeError:
        logger.error("Alliance page for id: " + alliance_id + " is not available.")
        return HttpResponseNotFound("<h1>Alliance Info Is Not Available</h1>")


@login_required
def listCorporations(request):
    template= loader.get_template('everegister/listEntities.html')
    corporations = Evecorporation.objects.all()
    context = RequestContext(request, {'title':'Corporation', 'type':'everegister:view_corporation', 'entities':corporations})
    return HttpResponse(template.render(context))


@login_required
def viewCorporation(request, corporation_id):
    try:
        template = loader.get_template('everegister/corporationProfile.html')
        eveapi = api.API()
        evecorp = corp.Corp(eveapi)
        corpdata = evecorp.corporation_sheet(corporation_id)
        context = RequestContext(request, {'corporation':corpdata})
        return HttpResponse(template.render(context))
    except APIError:
        logger.error("Corporation page for id: " + corporation_id + " is not available.")
        return HttpResponseNotFound("<h1>Corporation Info Is Not Available</h1>")


@login_required
def listCharacters(request):
    template= loader.get_template('everegister/listEntities.html')
    characters = Evecharacter.objects.all()
    context = RequestContext(request, {'title':'Character', 'type':'everegister:view_character', 'entities':characters})
    return HttpResponse(template.render(context))


@login_required
def viewCharacter(request, character_id):
    try:
        template = loader.get_template('everegister/characterProfile.html')
        characterobj = Evecharacter.objects.get(pk=character_id)
        eveapi = api.API(api_key=characterobj.api.get_auth())
        try:
            evecharacter = character.Char(api=eveapi, char_id=character_id)
            chardata = evecharacter.character_sheet()
            context = RequestContext(request, {'character':chardata})
            return HttpResponse(template.render(context))
        except APIError:
            logger.error("Character page for id: " + character_id + " is not available because of an API error.")
            return HttpResponseNotFound("<h1>Character Info Is Not Available</h1>")
    except (KeyError, Evecharacter.DoesNotExist):
        logger.error("Character page for id: " + character_id + " is not available because the requested character is not registered.")
        return HttpResponseNotFound("<h1>Character Is Not Registered</h1>")


# This is called once all the form data is validated
class RegisterWizard(SessionWizardView):
    def get_form(self, step=None, data=None, files=None):
        if step is None:
            step = self.steps.current

        # copied from super for custom methods
        # prepare the kwargs for the form instance.
        kwargs = self.get_form_kwargs(step)
        kwargs.update({
            'data': data,
            'files': files,
            'prefix': self.get_form_prefix(step, self.form_list[step]),
            'initial': self.get_form_initial(step),
        })
        if issubclass(self.form_list[step], forms.ModelForm):
            # If the form is based on ModelForm, add instance if available
            # and not previously set.
            kwargs.setdefault('instance', self.get_form_instance(step))
        elif issubclass(self.form_list[step], forms.models.BaseModelFormSet):
            # If the form is based on ModelFormSet, add queryset if available
            # and not previous set.
            kwargs.setdefault('queryset', self.get_form_instance(step))

        if step == '1':
            #do something special and really cool with the constructor
            apiKeyData = self.get_cleaned_data_for_step('0')['apiKeyData']
            kwargs.setdefault('apiKeyData', apiKeyData)
            pass

        #return the appropriate form
        return self.form_list[step](**kwargs)

    def done(self, form_list, **kwargs):
        clean_data = [form.cleaned_data for form in form_list]
        data = {}

        #import notices from each form for display at the end
        data['notices'] = []
        for form_data in clean_data:
            if 'notices' in form_data:
                data['notices'].extend(form_data['notices'])

        # Prepopulate useful values
        mainID = int(clean_data[1]['mainCharacter'])
        data['mainCharacter'] = clean_data[0]['apiKeyData']['characters'][mainID]
        data['username'] = clean_data[2]['username']
        data['password'] = clean_data[2]['password1']
        data['email'] = clean_data[2]['email']
        data['keyID'] = clean_data[0]['keyID']
        data['vCode'] = clean_data[0]['vCode']

        #register user based on provided values
        newUser = User.objects.create_user(username=data['username'], email=data['email'], password=data['password'])
        eveApi = update_eveapi(user=newUser, keyID=data['keyID'], vCode=data['vCode'], valid=True)

        #get character information
        charApi = api.API(api_key=(data['keyID'], data['vCode']))
        eveChar = character.Char(api=charApi, char_id=mainID)
        charSheet = eveChar.character_sheet()
        corpID = charSheet['corp']['id']
        allianceID = charSheet['alliance']['id']
        basicApi = api.API()

        #register alliance if there is one
        alliance = None
        if allianceID > 0:
            #get alliance info
            allianceList = basicApi.get('eve/AllianceList', {})
            allianceSearch = ".//*[@allianceID='" + allianceID.__str__() + "']"
            evealliance = allianceList.find(allianceSearch)
            alliance = update_evealliance(name=evealliance.attrib['name'], id=allianceID,
                                          ticker=evealliance.attrib['shortName'])

        #register corp
        evecorp = basicApi.get('corp/CorporationSheet', {'corporationID': corpID})
        _str, _int, _float, _bool, _ts = api.elem_getters(evecorp)
        corp = update_evecorporation(name=_str('corporationName'), id=corpID, ticker=_str('ticker'), alliance=alliance)

        #register character
        charRoles = ''
        charTitles = ''
        char = update_evecharacter(id=mainID, name=charSheet['name'], user=newUser, titles=charTitles, roles=charRoles,
                                   corporation=corp, api=eveApi)

        #run plugins

        #add results from plugin run to data

        # Render the success page
        return render_to_response('everegister/complete.html', {'form_data': data, })


class ChangeApiWizard(SessionWizardView):
    def get_form(self, step=None, data=None, files=None):
        if step is None:
            step = self.steps.current

        # copied from super for custom methods
        # prepare the kwargs for the form instance.
        kwargs = self.get_form_kwargs(step)
        kwargs.update({
            'data': data,
            'files': files,
            'prefix': self.get_form_prefix(step, self.form_list[step]),
            'initial': self.get_form_initial(step),
        })
        if issubclass(self.form_list[step], forms.ModelForm):
            # If the form is based on ModelForm, add instance if available
            # and not previously set.
            kwargs.setdefault('instance', self.get_form_instance(step))
        elif issubclass(self.form_list[step], forms.models.BaseModelFormSet):
            # If the form is based on ModelFormSet, add queryset if available
            # and not previous set.
            kwargs.setdefault('queryset', self.get_form_instance(step))

        if step == '1':
            #do something special and really cool with the constructor
            apiKeyData = self.get_cleaned_data_for_step('0')['apiKeyData']
            kwargs.setdefault('apiKeyData', apiKeyData)
            pass

        #return the appropriate form
        return self.form_list[step](**kwargs)

    def done(self, form_list, **kwargs):
        clean_data = [form.cleaned_data for form in form_list]
        data = {}

        #import notices from each form for display at the end
        data['notices'] = []
        for form_data in clean_data:
            if 'notices' in form_data:
                data['notices'].extend(form_data['notices'])

        # Prepopulate useful values
        mainID = int(clean_data[1]['mainCharacter'])
        data['mainCharacter'] = clean_data[0]['apiKeyData']['characters'][mainID]
        data['keyID'] = clean_data[0]['keyID']
        data['vCode'] = clean_data[0]['vCode']

        # Get the current user
        currentUser = self.request.user

        #Update Api with the new information
        eveApi = currentUser.eveapi
        eveApi.keyID = data['keyID']
        eveApi.vCode = data['vCode']
        eveApi.valid = True
        eveApi.save()

        #get character information
        charApi = api.API(api_key=(data['keyID'], data['vCode']))
        eveChar = character.Char(api=charApi, char_id=mainID)
        charSheet = eveChar.character_sheet()
        corpID = charSheet['corp']['id']
        allianceID = charSheet['alliance']['id']
        basicApi = api.API()

        #update or register alliance if there is one
        alliance = None
        if allianceID > 0:
            #get alliance info
            allianceList = basicApi.get('eve/AllianceList', {})
            allianceSearch = ".//*[@allianceID='" + allianceID.__str__() + "']"
            evealliance = allianceList.find(allianceSearch)
            alliance = update_evealliance(name=evealliance.attrib['name'], id=allianceID,
                                          ticker=evealliance.attrib['shortName'])

        #update or register corp
        evecorp = basicApi.get('corp/CorporationSheet', {'corporationID': corpID})
        _str, _int, _float, _bool, _ts = api.elem_getters(evecorp)
        corp = update_evecorporation(name=_str('corporationName'), id=corpID, ticker=_str('ticker'), alliance=alliance)

        #update or register character
        charRoles = ''
        charTitles = ''
        char = update_evecharacter(id=mainID, name=charSheet['name'], user=currentUser, titles=charTitles, roles=charRoles,
                                   corporation=corp, api=eveApi)

        #return user to the accounts page
        return HttpResponseRedirect(reverse("profile"))

