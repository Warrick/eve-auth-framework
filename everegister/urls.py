__author__ = 'drew'

from django.conf.urls import patterns, url
from everegister import views
from everegister.forms import EveApiForm, EveMainCharacterForm, EveRegisterForm

urlpatterns = patterns('',
                       # The registration process wizard
                       url(r'^register$', views.RegisterWizard.as_view
                           ([
                           EveApiForm,
                           EveMainCharacterForm,
                           EveRegisterForm,
                       ])
                           , name='register'),

                       #Change the API Key for the current user
                       url(r'^change_api$', views.ChangeApiWizard.as_view([
                           EveApiForm,
                           EveMainCharacterForm,
                          ]),
                          name='change_api'
                       ),

                       #Change the Main Character for the current user

                       #Display A list of known characters
                       url(r'^alliance/$', views.listAlliances, name="list_alliances"),

                       #Display information about an alliance
                       url(r'^alliance/(?P<alliance_id>\d+)/$', views.viewAlliance, name= "view_alliance"),

                       #Display A list of known characters
                       url(r'^corporation/$', views.listCorporations, name="list_corporations"),
                       #Display information about a corp
                       url(r'^corporation/(?P<corporation_id>\d+)/$', views.viewCorporation, name= "view_corporation"),

                       #Display A list of known characters
                       url(r'^character/$', views.listCharacters, name="list_characters"),

                       #Display information about a known character
                       url(r'^character/(?P<character_id>\d+)/$', views.viewCharacter, name= "view_character"),
)
