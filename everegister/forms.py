__author__ = 'drew'

from django import forms
from django.utils.safestring import mark_safe
from django.contrib.auth.models import User
from everegister import settings, validation, utils
from evelink import api, account
from evelink.api import APIError
import re

class EveApiForm(forms.Form):
    keyID = forms.IntegerField()
    vCode = forms.CharField()

    def clean(self):
        errors = []
        data = self.cleaned_data

        try:
            keyID = data['keyID']
        except KeyError:
            raise forms.ValidationError("The keyID must by numerical")

        try:
            vCode = data['vCode']
        except KeyError:
            raise forms.ValidationError("The vCode does not appear to be valid")

        result = utils.is_validApiKey(keyID, vCode)
        errors.extend(result['errors'])

        if result['valid']:
            eveApi = result['api']
            eveAccount = result['account']
            keyInfo = result['data']

            #Check that at least one character is allowed to register
            if not validation.has_validCharacter(keyInfo['characters']):
                errors.append('A valid character was not found for registration. '
                              'If you believe this notification is in error, contact your recruitment officer.')

        if len(errors) > 0:
            raise forms.ValidationError(errors)

        # Add the key and keyData to the now clean data
        data['apiKeyData'] = keyInfo
        data['apiKey'] = (keyID, vCode)
        return data


class EveMainCharacterForm(forms.Form):
    mainCharacter = forms.ChoiceField(label="Main Character")

    # Override to dynamically populate the mainCharacter field
    def __init__(self, apiKeyData, *args, **kwargs):
        #Get valid main characters
        validChars = validation.get_validCharacters(apiKeyData['characters'])
        validNames = []
        for key in validChars:
            validNames.append((key, validChars[key]['name']))
            #Create the form so we can access the currently available models
        super(EveMainCharacterForm, self).__init__(*args, **kwargs)
        self.fields['mainCharacter']._set_choices(validNames)
        

class EveRegisterForm(forms.Form):
    username = forms.CharField(max_length=30)
    email = forms.EmailField()
    password1 = forms.CharField(min_length=8, max_length=32, widget=forms.PasswordInput(), label="Password")
    password2 = forms.CharField(min_length=8, max_length=32, widget=forms.PasswordInput(), label="Verify Password")

    def clean(self):
        data = self.cleaned_data

        errors = []
        sameEmail = False
        sameUsername = False

        #notices that will be displayed on the completion screen
        notices = []

        #UserNames may not have spaces
        if ' ' in data['username']:
            oldName = data['username']
            data['username'] = oldName.replace(' ', '_')
            notices.append(mark_safe(
                'Usernames may not contain spaces. Your username has been changed to "<b><i>' + data[
                    'username'] + '</i></b>" from "<b><i>' + oldName + '</i></b>".'))

        #Make sure the username complies with the rest of the username requirments
        pattern = re.compile("^[a-zA-Z0-9_]*$")
        if not pattern.match(data['username']):
            errors.append("Usernames may only be alphanumeric and contain underscores")

        #Check for already existing email addresses
        try:
            alreadyRegisteredEmail = User.objects.get(email=data['email'])
            if alreadyRegisteredEmail:
                sameEmail = True
                errors.append("A user with that email already exists")
        except User.DoesNotExist:
            pass
        except KeyError:
            return

        #Check for already existing usernames
        try:
            alreadyRegisteredUsername = User.objects.get(username=data['username'])
            if alreadyRegisteredUsername:
                sameUsername = True
                errors.append("A user with that username already exists")
        except User.DoesNotExist:
            pass

        #Passwords must match
        if "password1" in data and "password2" in data and data["password1"] != data["password2"]:
            errors.append("Passwords must be the same")

        if len(errors) > 0:
            if sameEmail and sameUsername:
                errors.append(
                    "You have the same username and email as one of the current members. You may want to try logging in, instead of registering")
            raise forms.ValidationError(errors)

        data['notices'] = notices
        return data
