__author__ = 'drew'

from everegister import settings
from evelink import corp, api

# Check if at least one in a dictionary of characters are valid
def has_validCharacter(characters):
    for key in characters:
        character = characters[key]
        if is_validCharacter(character):
            return True
    return False


# Check if an individual character is a valid character
def is_validCharacter(character):
    # Check against valid alliances
    if len(settings.Valid_For_Registration['alliance']) > 0:
        eveCorp = corp.Corp(api.API())
        eveCorpInfo = eveCorp.corporation_sheet(corp_id=character['corp']['id'])
        if settings.Valid_For_Registration['alliance'].__contains__(eveCorpInfo['alliance']['name']):
            return True
            # Check against valid corporations
    if len(settings.Valid_For_Registration['corp']) > 0:
        if settings.Valid_For_Registration['corp'].__contains__(character['corp']['name']):
            return True
            # Check against valid characters
    if len(settings.Valid_For_Registration['character']) > 0:
        if settings.Valid_For_Registration['character'].__contains__(character['name']):
            return True
    return False


# Get a dictionary of all valid characters from a list
def get_validCharacters(characters):
    validCharacters = {}
    for key in characters:
        character = characters[key]
        if is_validCharacter(character):
            validCharacters[key] = character
    return validCharacters