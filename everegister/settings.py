# Minimum Access Mask
REGISTRATION_ACCESS_MASK = 59646280

# Link to create a new API key
REGISTRATION_CREATE_API_LINK = "http://support.eveonline.com/api/Key/CreatePredefined/{0}/".format(
    REGISTRATION_ACCESS_MASK)

# Dictionary of valid entities for registration
# Entries are case sensitive
Valid_For_Registration = {
    'alliance': [
    ],
    'corp': [
        'Hellion Reloaded',
    ],
    'character': [
    ],
}

