# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Eveapi'
        db.create_table(u'everegister_eveapi', (
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
            ('keyID', self.gf('django.db.models.fields.IntegerField')(primary_key=True)),
            ('vCode', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('valid', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'everegister', ['Eveapi'])

        # Adding model 'Evecorporation'
        db.create_table(u'everegister_evecorporation', (
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('id', self.gf('django.db.models.fields.IntegerField')(primary_key=True)),
            ('ticker', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('alliance', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['everegister.Evealliance'], null=True)),
        ))
        db.send_create_signal(u'everegister', ['Evecorporation'])

        # Adding model 'Evealliance'
        db.create_table(u'everegister_evealliance', (
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('id', self.gf('django.db.models.fields.IntegerField')(primary_key=True)),
            ('ticker', self.gf('django.db.models.fields.CharField')(max_length=15)),
        ))
        db.send_create_signal(u'everegister', ['Evealliance'])

        # Adding model 'Evecharacter'
        db.create_table(u'everegister_evecharacter', (
            ('id', self.gf('django.db.models.fields.IntegerField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('corporation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['everegister.Evecorporation'])),
            ('titles', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('roles', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('api', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['everegister.Eveapi'])),
        ))
        db.send_create_signal(u'everegister', ['Evecharacter'])


    def backwards(self, orm):
        # Deleting model 'Eveapi'
        db.delete_table(u'everegister_eveapi')

        # Deleting model 'Evecorporation'
        db.delete_table(u'everegister_evecorporation')

        # Deleting model 'Evealliance'
        db.delete_table(u'everegister_evealliance')

        # Deleting model 'Evecharacter'
        db.delete_table(u'everegister_evecharacter')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'everegister.evealliance': {
            'Meta': {'object_name': 'Evealliance'},
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'ticker': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        u'everegister.eveapi': {
            'Meta': {'object_name': 'Eveapi'},
            'keyID': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'}),
            'vCode': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'valid': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'everegister.evecharacter': {
            'Meta': {'object_name': 'Evecharacter'},
            'api': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['everegister.Eveapi']"}),
            'corporation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['everegister.Evecorporation']"}),
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'roles': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'titles': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'everegister.evecorporation': {
            'Meta': {'object_name': 'Evecorporation'},
            'alliance': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['everegister.Evealliance']", 'null': 'True'}),
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'ticker': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        }
    }

    complete_apps = ['everegister']