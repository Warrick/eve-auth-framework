from everegister.models import Eveapi, Evecharacter, Evecorporation, Evealliance
from everegister import constants, settings
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe
from evelink import api, account
from evelink.api import APIError

def update_eveapi(user, keyID, vCode, valid):
    key = None
    try:
        key = Eveapi.objects.get(keyID=keyID)
        key.user = user
        key.vCode = vCode
        key.valid = valid
        key.save()
    except Eveapi.DoesNotExist:
        #Save the api as new
        key = Eveapi(user=user, keyID=keyID, vCode=vCode, valid=valid)
        key.save()
    return key


def update_evecharacter(user, id, name, corporation, titles, roles, api):
    char = None
    try:
        char = Evecharacter.objects.get(pk=id)
        char.user = user
        char.corporation = corporation
        char.titles = titles
        char.roles = roles
        char.api = api
        char.save()
    except Evecharacter.DoesNotExist:
        char = Evecharacter(user=user, id=id, name=name, corporation=corporation, titles=titles, roles=roles, api=api)
        char.save()
    return char


def update_evecorporation(id, name, ticker, alliance):
    corp = None
    try:
        corp = Evecorporation.objects.get(pk=id)
        corp.name = name
        corp.ticker = ticker
        corp.alliance = alliance
        corp.save()
    except Evecorporation.DoesNotExist:
        corp = Evecorporation(id=id, name=name, ticker=ticker, alliance=alliance)
        corp.save()
    return corp


def update_evealliance(id, name, ticker):
    alliance = None
    try:
        alliance = Evealliance.objects.get(pk=id)
        alliance.name = name
        alliance.ticker = ticker
        corp.save()
    except Evealliance.DoesNotExist:
        alliance = Evealliance(id=id, name=name, ticker=ticker)
        alliance.save()
    return alliance

#Produces a bit field of minimum length <length> from <n> and optionally reversed if <reverse> = True
def bitField(n, length=None, reverse=False):
    bits = [1 if digit=='1' else 0 for digit in bin(n)[2:]]
    if length and len(bits) < length:
        lenDiff = length - len(bits)
        for x in range(0, lenDiff):
            bits.insert(0,0)
    if reverse:
        bits = bits[::-1]
    return bits

def get_invalidApiMaskProperties(apiMask):
    invalidProperties = []
    #get required bits
    reqBits = bitField(settings.REGISTRATION_ACCESS_MASK, 32, True)
    #perform introspection on the apiMask bits
    bits = bitField(apiMask, 32, True)
    
    #compare the required bits and the provided bits to produce the errors
    bitPow = 1
    for pos in range(0,32):
        if reqBits[pos] > bits[pos]:
            invalidProperties.append(constants.ACCESSMASK[bitPow])
        bitPow *= 2

    return invalidProperties

#Check the apiMask to see if meets the minimum requirements
def is_validApiMask(apiMask):
    errors = []
    valid = True
    if apiMask < settings.REGISTRATION_ACCESS_MASK:
        valid = False
        errors.append('The current API Key access mask is too restrictive. '
                        'The access mask is currently "' + apiMask.__str__() +
                        '", it needs to be greater than "'
                        + settings.REGISTRATION_ACCESS_MASK.__str__() + '"')
        #append the missing characteristics as a sub list
        missingCharacteristics = get_invalidApiMaskProperties(apiMask)
        missingCharacteristicsMessage = "Missing Key Characteristics:<ul>"
        for characteristic in missingCharacteristics:
            missingCharacteristicsMessage += "<li>" + characteristic + "</li>"
        missingCharacteristicsMessage += "</ul>"
        errors.append(mark_safe(missingCharacteristicsMessage))

    return {'valid':valid, 'errors':errors}

# Check if an api key is valid, if so return the apikey data
def is_validApiKey(keyID, vCode):
    isValid = False
    apiKeyData = None
    apiError = False
    errors = []
    
    #Create a reference to the api from and attempt to initialize
    eveApi = api.API(api_key=(keyID, vCode))
    eveAccount = account.Account(eveApi)

    try:
        keyInfo = eveAccount.key_info()

        # Check that the data is valid
        if not keyInfo:
            raise APIError("The provided API Key was invalid")

        #Check that the key is an account key and not a character key
        if keyInfo['type'] is not "account":
            apiError = True
            errors.append('The provided API Key was a "' + keyInfo['type']
                            + '" key, not an "account" key.'
                            ' Character keys and corp keys are not accepted.')

        #Check that the key does not expire
        if keyInfo['expire_ts'] is not None:
            apiError = True
            errors.append('The current API Key is set to expire on '
                            + api.readable_ts(keyInfo['expire_ts'])
                            + '. The API Key must NOT expire!')

        #Check that the registration mask meets the minimum requirements
        validApiMask = is_validApiMask(keyInfo['access_mask'])
        if not validApiMask['valid']:
            apiError = True
            errors.extend(validApiMask['errors'])

        #Make the key info available if we passed all the tests
        apiKeyData = keyInfo

    except APIError:
        apiError = True
        errors.append("The API key was invalid or the EVE API servers were inaccessible.")

    if apiError:
        # Add an error that contains a link to a create a proper key
        errors.append(mark_safe('One or more API validation steps failed. '
                                'A proper key can be created '
                                '<a href="'
                                + settings.REGISTRATION_CREATE_API_LINK +
                                '">here</a>. '
                                'Remember to check the "No Expiry" check box, give it a name, and submit it!'
            ))

    if len(errors) < 1:
        isValid = True

    return {'valid':isValid, 'data':apiKeyData, 'api':eveApi, 'account':eveAccount, 'errors':errors}

#Returns a tuple of valid and invalid users according to the rules
def get_userList():
    #lists of users
    validUsers = []
    invalidUsers = []

    users = User.objects.all()

    for user in users:
        if not user.is_active:
            invalidUsers.append(user)
        else:
            #User is active, new we need to check api key and characters
            pass        

    return (validUsers, invalidUsers)
