from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Eveapi(models.Model):
    user = models.OneToOneField(User)
    keyID = models.IntegerField(primary_key=True)
    vCode = models.CharField(max_length=255)
    valid = models.BooleanField()

    def get_auth(self):
        return (self.keyID, self.vCode)

    def __unicode__(self):
        return self.user.username


class Evecorporation(models.Model):
    name = models.CharField(max_length=100)
    id = models.IntegerField(primary_key=True)
    ticker = models.CharField(max_length=15)
    alliance = models.ForeignKey('Evealliance', null=True)

    def __unicode__(self):
        return self.name


class Evealliance(models.Model):
    name = models.CharField(max_length=100)
    id = models.IntegerField(primary_key=True)
    ticker = models.CharField(max_length=15)

    def __unicode__(self):
        return self.name


class Evecharacter(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100)
    user = models.OneToOneField(User)
    corporation = models.ForeignKey('Evecorporation')
    titles = models.CharField(max_length=255, null=True)
    roles = models.CharField(max_length=255, null=True)
    api = models.ForeignKey('Eveapi')

    def __unicode__(self):
        return self.name
