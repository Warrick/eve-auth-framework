from django.contrib import admin
from everegister.models import Eveapi, Evecharacter, Evecorporation, Evealliance

admin.site.register(Eveapi)
admin.site.register(Evecharacter)
admin.site.register(Evecorporation)
admin.site.register(Evealliance)
