from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
import views

# Uncomment the next two lines to enable the admin:
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       # This is the home page
                       url(r'^$', views.index, name='index'),
                       url(r'^accounts/login/$', 'django.contrib.auth.views.login', {}, name='login'),
                       url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {}, name='logout'),

                       # Profile Page
                       url(r'accounts/profile/', views.profile, name='profile'),

                       # EVE Api Proxy Endpoint
                       url(r'^api/', include('evelink.urls')),

                       # EVE Registration Endpoint
                       url(r'^eve/', include('everegister.urls', namespace="everegister")),

                       # url(r'^EAF/', include('EAF.foo.urls')),

                       # Uncomment the admin/doc line below to enable admin documentation:
                       url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

                       # Uncomment the next line to enable the admin:
                       url(r'^admin/', include(admin.site.urls)),
                        
                       # Password Reset Url's
                       url(r'^accounts/password/reset/$',
                           'django.contrib.auth.views.password_reset',
                           {'post_reset_redirect': '/accounts/password/reset/done/'},
                           name="password_reset"),

                       url(r'^accounts/password/reset/done/$',
                           'django.contrib.auth.views.password_reset_done'),

                       url(r'^accounts/password/reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',
                           'django.contrib.auth.views.password_reset_confirm',
                           {'post_reset_redirect': '/accounts/password/done/'}),

                       url(r'^accounts/password/done/$',
                           'django.contrib.auth.views.password_reset_complete'),

                       #Password Change Url's
                       url(r'^accounts/password_change/$', 
                           'django.contrib.auth.views.password_change',
                           name="password_change"),

                       url(r'^accounts/', include('django.contrib.auth.urls')),
)

#import the static urls for development
urlpatterns += staticfiles_urlpatterns()
