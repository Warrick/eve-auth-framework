from django.template import RequestContext, loader
from django.contrib.auth.decorators import login_required

__author__ = 'drew'

#Add views here

from django.http import HttpResponse


# home page with registration and login dialogs
def index(request):
    template = loader.get_template('EAF/index.html')
    context = RequestContext(request, {})
    return HttpResponse(template.render(context))

# user profile page
@login_required
def profile(request):
    template = loader.get_template('EAF/profile.html')
    context = RequestContext(request, {})
    return HttpResponse(template.render(context))
