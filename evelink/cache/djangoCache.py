import time
from django.core.cache import cache
from evelink import api

class DjangoCache(api.APICache):
    """An implementation of APICache using the django framework caching."""

    def __init__(self):
        pass

    def get(self, key):
        return cache.get(key) 

    def put(self, key, value, duration):
        cache.set(key,value,duration)
