__author__ = 'drew'

from django.conf.urls import patterns, url
from evelink import views

urlpatterns = patterns('',
                       url(r'^', views.api, name='api')
)
