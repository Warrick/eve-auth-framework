# Create your views here.
from django.http import HttpResponse
from evelink import api as eveapi


def api(request):
    # This is the URL path (minus the parameters).
    url_path = request.META['PATH_INFO']

    # Clean the url path
    url_path = url_path[5:]

    # Get auth data if present
    userID = None
    vCode = None
    try:
        userID = request.GET.get('userID')
        vCode = request.GET.get('vCode')
    except KeyError:
        pass

    # Convert the leftover params into a dictionary
    params = {}
    for param in request.GET.lists():
        if param[0] is 'userID' or param[0] is 'vCode':
            pass
        if len(param[1]) > 1:
            params[param[0]] = param[1]
        else:
            params[param[0]] = param[1][0]

    api = None
    if userID is not None and vCode is not None:
        api = eveapi.API(api_key=(userID, vCode))
    else:
        api = eveapi.API()

    result = None
    if len(params) >= 1:
        result = api.getRaw(url_path, params)
    else:
        result = api.getRaw(url_path)

    # requestData = "" + url_path + " : " + params.__str__()

    return HttpResponse(result, mimetype="application/xml")
