"""EVELink - Python bindings for the EVE API."""

import logging

from evelink import account
from evelink import api
from evelink import char
from evelink import constants
from evelink import corp
from evelink import eve
from evelink import map
from evelink import server

# Import for the default cache
from evelink.cache import djangoCache

__version__ = "0.2.0"

# Implement NullHandler because it was only added in Python 2.7+.
class NullHandler(logging.Handler):
    def emit(self, record):
        pass

# Create a logger, but by default, have it do nothing
_log = logging.getLogger('evelink')
_log.addHandler(NullHandler())

__all__ = [
  "account",
  "api",
  "char",
  "constants",
  "corp",
  "eve",
  "map",
  "parsing",
  "server",
]

# Set the default cache
api.set_defaultcache(djangoCache.DjangoCache())
